# VPC

resource "aws_vpc" "node_app_vpc" {
  cidr_block           = "10.0.0.0/16"
  instance_tenancy     = "default"
  enable_dns_support   = "true"
  enable_dns_hostnames = "true"
  enable_classiclink   = "false"

   tags = {
    Name = "nodeapp_vpc"
  }
}

# Public subnets

resource "aws_subnet" "node_app_public_a" {
  vpc_id                  = aws_vpc.node_app_vpc.id
  cidr_block              = "10.0.1.0/24"
  map_public_ip_on_launch = "true"
  availability_zone       = "eu-central-1a"

  tags = {
    Name = "nodeapp_pub_a"
  }
}

resource "aws_subnet" "node_app_public_b" {
  vpc_id                  = aws_vpc.node_app_vpc.id
  cidr_block              = "10.0.2.0/24"
  map_public_ip_on_launch = "true"
  availability_zone       = "eu-central-1b"

   tags = {
    Name = "nodeapp_pub_b"
  }
}

# Private subnets

resource "aws_subnet" "node_app_private_a" {
  vpc_id                  = aws_vpc.node_app_vpc.id
  cidr_block              = "10.0.3.0/24"
  map_public_ip_on_launch = "false"
  availability_zone       = "eu-central-1a"
  
  tags = {
    Name = "nodeapp_priv_a"
  }
}

resource "aws_subnet" "node_app_private_b" {
  vpc_id                  = aws_vpc.node_app_vpc.id
  cidr_block              = "10.0.4.0/24"
  map_public_ip_on_launch = "false"
  availability_zone       = "eu-central-1b"
  
  tags = {
    Name = "nodeapp_priv_b"
  }
}

# Internet GW

resource "aws_internet_gateway" "nodeapp_gw" {
  vpc_id = aws_vpc.node_app_vpc.id

  tags = {
    Name = "nodeapp_igw"
  }
}

# Routing tables

resource "aws_route_table" "nodeapp_routing_public" {
  vpc_id = aws_vpc.node_app_vpc.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.nodeapp_gw.id
  }

  tags = {
    Name = "main-public-1"
  }
}

# route associations public-1a

resource "aws_route_table_association" "nodeapp-public-1a" {
  subnet_id      = aws_subnet.node_app_public_a.id
  route_table_id = aws_route_table.nodeapp_routing_public.id
}

# route associations public-1b

resource "aws_route_table_association" "nodeapp-public-1b" {
  subnet_id      = aws_subnet.node_app_public_b.id
  route_table_id = aws_route_table.nodeapp_routing_public.id

}