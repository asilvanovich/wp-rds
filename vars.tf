variable "AWS_REGION" {
  default = "eu-central-1"
}

variable "AMIS" {
  type = map(string)
  default = {
    eu-central-1 = "ami-01933d3dbcb8f63e0"
  }
}

variable "RDS_PASSWORD" {
  type = string
}