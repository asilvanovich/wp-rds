resource "aws_lb" "nodeapp_alb" {
  name               = "nodeappalb"
  internal           = false
  load_balancer_type = "application"
  security_groups    = [aws_security_group.secgroup_alb.id]
  subnets            = [aws_subnet.node_app_public_a.id, aws_subnet.node_app_public_b.id]

  tags = {
    Environment = "production"
  }
}

resource "aws_lb_target_group" "nodeapp_alb_target" {
  name     = "nodeapp-alb-target"
  port     = 80
  protocol = "HTTP"
  vpc_id   = aws_vpc.node_app_vpc.id

  health_check {
    protocol = "HTTP"
    path     = "/" 
    port     = "80"
    matcher  = "200"
  }
}

resource "aws_launch_configuration" "nodeapp_launch_conf" {
  name          = "nodeapp_conf"
  image_id      = var.AMIS[var.AWS_REGION]
  instance_type = "t2.micro"
  security_groups = [aws_security_group.secgroup_ec2.id]
  user_data       = file("install.sh")

}

resource "aws_autoscaling_group" "nodeapp_autoscaling" {
  name                 = "terraform-asg-example"
  force_delete         = true
  launch_configuration = aws_launch_configuration.nodeapp_launch_conf.name
  min_size             = 1
  max_size             = 1
  vpc_zone_identifier  = [aws_subnet.node_app_private_a.id]

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_autoscaling_attachment" "nod_asg_attachment" {
  autoscaling_group_name = aws_autoscaling_group.nodeapp_autoscaling.id
  alb_target_group_arn   = aws_lb_target_group.nodeapp_alb_target.arn

}

resource "aws_lb_listener" "node_app" {
  load_balancer_arn = aws_lb.nodeapp_alb.arn
  port              = "80"
  protocol          = "HTTP"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.nodeapp_alb_target.arn
  }
}