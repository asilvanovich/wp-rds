# nat gw
resource "aws_eip" "nat_ip" {
  vpc = true
}

resource "aws_nat_gateway" "nat_gw" {
  allocation_id = aws_eip.nat_ip.id
  subnet_id     = aws_subnet.node_app_public_a.id
  depends_on    = [aws_internet_gateway.nodeapp_gw]
}

# Route setup for NAT
resource "aws_route_table" "route_private_a" {
  vpc_id = aws_vpc.node_app_vpc.id
  route {
    cidr_block     = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.nat_gw.id
  }

  tags = {
    Name = "nodeapp_route_priv_a"
  }
}

# route associations private
resource "aws_route_table_association" "nodeapp_private_1a" {
  subnet_id      = aws_subnet.node_app_private_a.id
  route_table_id = aws_route_table.route_private_a.id
}

resource "aws_route_table_association" "nodeapp_private_1b" {
  subnet_id      = aws_subnet.node_app_private_b.id
  route_table_id = aws_route_table.route_private_a.id
}


