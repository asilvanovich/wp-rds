#!/bin/bash

#Variables
secret="docker_password"

#Install unzip
sudo yum install unzip

#Install aws-cli and login Docker
aws ssm get-parameters-by-path --region=eu-central-1 --path / --output text --query "Parameters[].[Name, Value]" | grep "$secret" | cut -f2 | docker login --username leverxlearning --password-stdin
curl "https://s3.amazonaws.com/aws-cli/awscli-bundle.zip" -o "awscli-bundle.zip"
unzip awscli-bundle.zip
sudo ./awscli-bundle/install -i /usr/local/aws -b /usr/local/bin/aws

#Install gitlab-runner
echo 'installing gitlab-runner'
curl -L https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.rpm.sh | sudo bash
sudo yum install gitlab-runner -y


#Register gitlab-runner
echo 'registering gitlab-runner'
sudo gitlab-runner register --non-interactive --url "https://gitlab.leverx-group.com" \
--registration-token "kyTnZcnf_xvjAYjgyFqP" \
--executor "shell" \
--description "docker-runner" \
--tag-list "docker,maven,build,deploy" \
--run-untagged="true"
echo "gitlab-runner ALL=(ALL) NOPASSWD: ALL" > /etc/sudoers.d/gitlab-runner
usermod -aG docker gitlab-runner
echo 'setup complete!'
