resource "aws_db_subnet_group" "mysql_subnet" {
  name        = "mysql-subnet"
  description = "RDS subnet group"
  subnet_ids  = [aws_subnet.node_app_private_a.id, aws_subnet.node_app_private_b.id]
}

resource "aws_db_parameter_group" "mysql_parameters" {
  name        = "mysql-parameters"
  family      = "mysql5.7"
  description = "MySQL parameter group"

}

resource "aws_db_instance" "mariadb" {
  allocated_storage       = 100 
  engine                  = "mysql"
  engine_version          = "5.7.26"
  instance_class          = "db.t2.micro"
  identifier              = "mysql"
  name                    = "Nameformydb"
  username                = "root"          
  password                = var.RDS_PASSWORD
  db_subnet_group_name    = aws_db_subnet_group.mysql_subnet.name
  parameter_group_name    = aws_db_parameter_group.mysql_parameters.name
  multi_az                = "false"
  vpc_security_group_ids  = [aws_security_group.secgroup_mysql.id]
  storage_type            = "gp2"
  backup_retention_period = 30
  availability_zone       = aws_subnet.node_app_private_a.availability_zone
  skip_final_snapshot     = true                                       
  tags = {
    Name = "mysql-instance"
  }
}
