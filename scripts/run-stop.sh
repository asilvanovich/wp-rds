#!/bin/bash

if docker container ls | grep -q 'node-cont'; then
 sudo docker stop node-cont
 sudo docker rm node-cont
fi


docker run -d -p 80:3000 --name node-cont nodejsapp:latest
