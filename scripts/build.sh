#!/bin/bash
set -e
image="nodejsapp"

#get timestamp for the tag
gitcom=$(git log -1 --pretty=%h)

tag=$image:$gitcom
#build image
sudo docker build -t $tag .

#tag and push as latest
taglat=$image:latest
sudo docker tag  $tag $taglat
