#!/bin/bash
echo 'starting setup...'
echo 'installing gitlab-ci-multi-runner repository'
curl -L https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.rpm.sh | sudo bash
echo 'installing gitlab-ci-multi-runner'
sudo yum install gitlab-runner -y
echo 'registering gitlab-ci-multi-runner'
sudo gitlab-runner register --non-interactive --url "https://gitlab.leverx-group.com" \
--registration-token "kyTnZcnf_xvjAYjgyFqP" \
--executor "shell" \
--description "docker-runner" \
--tag-list "docker,maven,build,deploy" \
--run-untagged="true"
echo "gitlab-runner ALL=(ALL) NOPASSWD: ALL" > /etc/sudoers.d/gitlab-runner
usermod -aG docker gitlab-runner
echo 'setup complete!'
