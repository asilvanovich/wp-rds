resource "aws_security_group" "secgroup_alb" {
  vpc_id      = aws_vpc.node_app_vpc.id
  name        = "allow-http_alb"
  description = "security group that allows ingress http and all egress traffic"
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = {
    Name = "allow-http-alb"
  }
}

resource "aws_security_group" "secgroup_ec2" {
  vpc_id      = aws_vpc.node_app_vpc.id
  name        = "allow-http_ec2"
  description = "security group that allows ingress http and all egress traffic"
  
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    security_groups = [aws_security_group.secgroup_alb.id]
  }
  tags = {
    Name = "allow-http-ec2"
  }
}

resource "aws_security_group" "secgroup_mysql" {
  vpc_id      = aws_vpc.node_app_vpc.id
  name        = "allow-3306-port"
  description = "security group that allows ingress traffic to 3306"
  
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 3306
    to_port     = 3306
    protocol    = "tcp"
    security_groups = [aws_security_group.secgroup_ec2.id]
  }
  tags = {
    Name = "allow-3306"
  }
}